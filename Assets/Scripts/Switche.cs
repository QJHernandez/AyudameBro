﻿using UnityEngine;
using System.Collections;

public class Switche : MonoBehaviour {

	public int idSwitch;
	public ControladorPrincipal2 scriptPrincipal;
	public ControlPersonaje scriptPersonaje;
	private bool activo;

	public Sprite[] sprites;
	public SpriteRenderer renderer;

	void Start(){
		activo = true;

	}
	// Update is called once per frame
	void Awake () {


	}

	public void resetear(){
		activo = true;
		//print ("switche" + idSwitch + " levantado, activo otra vez: " + activo);

		// Imagen de switch levantado
		renderer.sprite = sprites[0];
	}


	void OnTriggerEnter2D(Collider2D colision){
		if (colision.tag == "jugador" && activo){

			//print ("switch " + idSwitch + " presionado  " + activo);
			renderer.sprite = sprites[1];
			// Imagen de switch presionado
			//print("enviado switche al controlador principal");
			if (scriptPersonaje.idJugador == 1)
				scriptPrincipal.presionadoSwitchJugador1(idSwitch);
			else
				scriptPrincipal.presionadoSwitchJugador2(idSwitch);
			activo = false;
		}
	}

	void OnTriggerExit2D(Collider2D colision){

		if(colision.tag == "jugador"){


			scriptPrincipal.revisarSwitchesJugador();
		}
	}
}
