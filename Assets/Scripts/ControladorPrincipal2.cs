﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;

public class ControladorPrincipal2 : MonoBehaviour {

	public Text victoria;

	// Orden que debe cumplir cada jugador (Se genera aleatoriamente)
	private int[] ordenJugador1;
	private int[] ordenJugador2;

	// Switches que ha presionado cada jugador en orden
	private int[] switchesJugador1;
	private int[] switchesJugador2;

	// Indice actual del arreglo de switches de cada jugador
	private int switcheActualJ1;
	private int switcheActualJ2;

	// Numero total de switches
	private int numSwitches;

	// Lista de gameObjects switches
	public List<Switche> listaSwitchesJ1;
	public List<Switche> listaSwitchesJ2;

	private bool completadoJ1, completadoJ2;

	// Use this for initialization
	void Start () {
		completadoJ1 = false;
		completadoJ2 = false;

		numSwitches = 3;
		ordenJugador1 = new int[numSwitches];
		ordenJugador2 = new int[numSwitches];
		switchesJugador1 = new int[numSwitches];
		switchesJugador2 = new int[numSwitches];
		switcheActualJ1 = 0;
		switcheActualJ2 = 0;

		List<int> listaSwitches = new List<int>();

		// Genero el orden de los switches aleatoriamente para el jugador 1
		// Primero lleno una lista con numeros de 0 a numSwitches
		for(int i= 0; i < numSwitches; i++){
			listaSwitches.Add(i);
		}

		for(int i=0; i < numSwitches; i++){
			// Despues accedo aleatoriamente la lista para extraer(No eliminar) un switche cualquiera
			int switchExtraido = listaSwitches[Random.Range(0, listaSwitches.Count)];
			//print ("switch " +i + " para el jugador 1: " + switchExtraido);
			ordenJugador1[i] = switchExtraido;

			// Elimino el switche extraido para que no se vuelva a tomar en cuenta
			listaSwitches.Remove(switchExtraido);
		}

		// Genero el orden de los switches aleatoriamente para el jugador 2
		for(int i= 0; i < numSwitches; i++){
			listaSwitches.Add(i);
		}

		for(int i=0; i < numSwitches; i++){
			int switchExtraido = listaSwitches[Random.Range(0, listaSwitches.Count)];
			//print ("switch " +i + " para el jugador 2: " + switchExtraido);
			ordenJugador2[i] = switchExtraido;
			listaSwitches.Remove(switchExtraido);
		}
	}

	public void presionadoSwitchJugador1(int idSwitch){
		//print ("el controlador recibio el swiche " + idSwitch + " del jugador 1");
		if (switcheActualJ1 < numSwitches){
				switchesJugador1[switcheActualJ1] = idSwitch;
			switcheActualJ1++;}


		}

	public void presionadoSwitchJugador2(int idSwitch){
		//print ("el controlador recibio el swiche " + idSwitch + " del jugador 2");
			if (switcheActualJ2 < numSwitches){
			switchesJugador2[switcheActualJ2] = idSwitch;
			switcheActualJ2++;}

			


	}

	public void revisarSwitchesJugador(){
		//print ("switcheActualJ1: "+ switcheActualJ1 + ", switcheActualJ2: "+ switcheActualJ2);

		// ¿El jugador 1 ha presionado los switches en orden?
			if (!completadoJ1 && switcheActualJ1 == (numSwitches)){
			// Chequeando que cada switche este en el orden correcto
			bool iguales = true;
			for(int i=0; i < numSwitches; i++){
				if (switchesJugador1[i] != ordenJugador1[i]){
					iguales = false;
					break;
				}
			}

			if (iguales){
				completadoJ1 = true;
				//print ("presionados todos los switches del jugador 1 EN ORDEN CORRECTO");
			}
			else{
				// Objetivo no completado en el orden que es asi que se resetean los switches
				foreach (Switche scriptSwitche in listaSwitchesJ1){
					switchesJugador1 = new int[numSwitches];
					switcheActualJ1 = 0;
					scriptSwitche.resetear();
				}
			}
		} 

		// ¿El jugador 2 ha presionado los switches en orden?
			if (!completadoJ2 && switcheActualJ2 == (numSwitches)){
			bool iguales = true;
			for(int i=0; i < numSwitches; i++){
				if (switchesJugador2[i] != ordenJugador2[i]){
					iguales = false;
					break;
				}
			}

			if (iguales){
				completadoJ2 = true;
				//print ("presionados todos los switches del jugador 1 EN ORDEN CORRECTO");
			}
			else{
				// Objetivo no completado en el orden que es asi que se resetean los switches
				foreach (Switche scriptSwitche in listaSwitchesJ2){
					switchesJugador2 = new int[numSwitches];
					switcheActualJ2 = 0;
					scriptSwitche.resetear();
				}
			}
		}
		
		// ¿Los dos jugadores presionaron los switches en orden?
		if (completadoJ1 && completadoJ2)
		{
			victoria.enabled = true;

			Invoke ("CargarInicio", 3);
			print ("VICTORIA");
			}
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void CargarInicio(){
		Application.LoadLevel("MainMenu");
	}
}
