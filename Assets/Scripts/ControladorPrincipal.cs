﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ControladorPrincipal : MonoBehaviour {
	
	public Text victoryText;
	public GameObject[] objetosCasilla1;
	public GameObject casilla1;
	public GameObject[] objetosCasilla2;
	public GameObject casilla2;
	public GameObject[] objetosCasilla3;
	public GameObject casilla3;
	public GameObject[] objetosCasilla4;
	public GameObject casilla4;

	private Vector2 casPos1;
	private Vector2 casPos2;
	private Vector2 casPos3;
	private Vector2 casPos4;
	private Vector2 obj1Cas1Pos;
	private Vector2 obj2Cas1Pos;
	private Vector2 obj1Cas2Pos;
	private Vector2 obj2Cas2Pos;
	private Vector2 obj1Cas3Pos;
	private Vector2 obj2Cas3Pos;
	private Vector2 obj1Cas4Pos;
	private Vector2 obj2Cas4Pos;

	void Awake(){
		casPos1 = new Vector2 (casilla1.transform.position.x, casilla1.transform.position.y);
		casPos2 = new Vector2 (casilla2.transform.position.x, casilla2.transform.position.y);
		casPos3 = new Vector2 (casilla3.transform.position.x, casilla3.transform.position.y);
		casPos4 = new Vector2 (casilla4.transform.position.x, casilla4.transform.position.y);
	}

	void Start(){
	}
	
	void Update(){
		obj1Cas1Pos = new Vector2 (objetosCasilla1 [0].transform.position.x, objetosCasilla1 [0].transform.position.y);
		obj2Cas1Pos = new Vector2 (objetosCasilla1 [1].transform.position.x, objetosCasilla1 [1].transform.position.y);
		obj1Cas2Pos = new Vector2 (objetosCasilla2 [0].transform.position.x, objetosCasilla2 [0].transform.position.y);
		obj2Cas2Pos = new Vector2 (objetosCasilla2 [1].transform.position.x, objetosCasilla2 [1].transform.position.y);
		obj1Cas3Pos = new Vector2 (objetosCasilla3 [0].transform.position.x, objetosCasilla3 [0].transform.position.y);
		obj2Cas3Pos = new Vector2 (objetosCasilla3 [1].transform.position.x, objetosCasilla3 [1].transform.position.y);
		obj1Cas4Pos = new Vector2 (objetosCasilla4 [0].transform.position.x, objetosCasilla4 [0].transform.position.y);
		obj2Cas4Pos = new Vector2 (objetosCasilla4 [1].transform.position.x, objetosCasilla4 [1].transform.position.y);

		if ((obj1Cas1Pos == casPos1 || obj2Cas1Pos == casPos1) &&
		    (obj1Cas2Pos == casPos2 || obj2Cas2Pos == casPos2) &&
		    (obj1Cas3Pos == casPos3 || obj2Cas3Pos == casPos3) &&
		    (obj1Cas4Pos == casPos4 || obj2Cas4Pos == casPos4)) {				
			victoryText.enabled = true;
			Invoke("LoadLevel", 2);
		}
	}

	IEnumerator ChangeLevel(){
		float fadeTime = GameObject.Find ("_ControladorPrincipal").GetComponent<SceneFading> ().BeginFade (-1);
		yield return new WaitForSeconds (fadeTime);
		Application.LoadLevel("Nivel2");	
	}

	void LoadLevel(){
		Application.LoadLevel("Nivel2");
	}

}
