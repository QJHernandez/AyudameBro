﻿using UnityEngine;
using System.Collections;

public class ControlPersonaje : MonoBehaviour
{
	public KeyCode arriba;
	public KeyCode abajo;
	public KeyCode izquierda;
	public KeyCode derecha;
	public KeyCode accion;
	public int idJugador;

	private Transform objetoAMover;
	Animator anim;

	void Start(){
		anim = GetComponent<Animator>(); 
	}

		// Update is called once per frame
	void FixedUpdate (){

		if (Input.GetKey (arriba)) {
			transform.Translate (new Vector2 (0.0f, 0.1f));
			if (Input.GetKey (accion) && objetoAMover != null) {
				objetoAMover.transform.Translate (new Vector2 (0.0f, 0.1f));
			}
			anim.SetInteger("State",2);
		}
		if (Input.GetKey (abajo)) {
			transform.Translate (new Vector2 (0.0f, -0.1f));
			if (Input.GetKey (accion) && objetoAMover != null) {
				objetoAMover.transform.Translate (new Vector2 (0.0f, -0.1f));
			}
			anim.SetInteger("State",0);
		}
		if (Input.GetKey (derecha)) {
			transform.Translate (new Vector2 (0.1f, 0.0f));
			if (Input.GetKey (accion) && objetoAMover != null) {
				objetoAMover.transform.Translate (new Vector2 (0.1f, 0.0f));
			}
			anim.SetInteger("State",3);
		}
		if (Input.GetKey (izquierda)) {
			transform.Translate (new Vector2 (-0.1f, 0.0f));
			if (Input.GetKey (accion) && objetoAMover != null) {
				objetoAMover.transform.Translate (new Vector2 (-0.1f, 0.0f));
			}
			anim.SetInteger("State",1);
		}
				
	}

	void OnTriggerEnter2D (Collider2D colision){

		//Debug.Log ("colision: " + colision.tag);
		if (colision.tag == "objeto") {
			//print ("el personaje va a agarrar algo");
			objetoAMover = colision.transform;
		} 
	}

	void OnTriggerExit2D (Collider2D colision)
	{
		if (colision.tag == "objeto") {
			objetoAMover = null;
		}
	}

}
